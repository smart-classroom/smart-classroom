enum UserRole {
  Teacher = 1,
  Student = 2
};

export default UserRole;